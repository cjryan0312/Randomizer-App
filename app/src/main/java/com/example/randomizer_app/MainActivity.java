package com.example.randomizer_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickRandomPairs(View view) {
        /*
        TODO:
         */
        TextView pairs = (TextView) findViewById(R.id.result);
        String name1;
        String name2;
        String finalWord = "";
        ArrayList<String> names = new ArrayList<String>();
        Collections.addAll(names, "Jakob", "Amirah", "Lucas", "Miguel", "Leila",
                "Liam", "Jio", "Rachael");
        while (!names.isEmpty()) {
            Collections.shuffle(names);
            name1 = names.get(0);
            name2 = names.get(1);
            finalWord += name1 + " + " + name2 + " are a pair. \n";
            names.remove(1);
            names.remove(0);
        }
        pairs.setText(finalWord);
    }
}